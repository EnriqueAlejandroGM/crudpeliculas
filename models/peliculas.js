const mongoose = require('mongoose')

var schema = new mongoose.Schema ({
    movieName : {
        type: String
    },
    director: {
        type: String
    },
    releaseDate: {
        type: Date
    },
    publisher: {
        type: String
    },
    principalActors: [{
        type: Object
    }] 
})

var model = mongoose.model('Movies', schema)

module.exports = model