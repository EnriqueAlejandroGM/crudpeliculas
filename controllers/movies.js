const router = require('express').Router()

function findAll(req, res) {
    Model.find({})
    .exec()
    .then((docs) => {
        res.json(docs)
    })
    .catch((error) => {
        const mensaje = 'Error al pedir datos de la DB'
        console.log(mensaje, error)
        res.status(500).json({ message })
    })
}

function findOne(req, res) {
    Model.findById(req.params.id)
    .exec()
    .then((docs) => {
        res.json(docs)
    })
    .catch((error) => {
        const mensaje = 'Error al pedir datos de la DB'
        console.log(mensaje, error)
        res.status(500).json({ message })
    })
}

function createOne(req, res) {
    console.log(req.body)
    const nuevaPeli = {
        movieName: req.body.movieName,
        director: req.body.director,
        releaseDate: req.body.releaseDate,
        publisher: req.body.publisher,
        principalActors: req.body.principalActors
    }
    return new Model(nuevaPeli)
        .save()
        .then((doc) => {
            console.log('Película grabada en la base de datos')
            res.json(doc)
        })
        .catch((error) => {
            const message = 'Error al guardar la película en la base de datos'
            console.error(message, error)
            res.status(500).json({ message })
        })
}

function update(req, res) {
    Model.findByIdAndUpdate(req.params.id, { movieName: req.params.sustituto })
    .then((doc) => {
        console.log('Película con id: ' + req.params.id + ' actualizada.')
        res.send('Película con id: ' + req.params.id + ' actualizada.')
    })
    .catch((error) => {
        console.log(error)
        res.send(error)
    })
}

function del(req, res){
    Model.findByIdAndRemove(req.params.id)
    .then((doc) => {
        console.log('Película con id ' + req.params.id + ' eliminada.')
        res.send('Película con id ' + req.params.id + ' eliminada.')
    })
    .catch((error) => {
        console.log(error, 'Película no encontrada')
        res.send(error)
    })  
}

module.exports = {
    findAll,
    findOne,
    createOne,
    update,
    del
}