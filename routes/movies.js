var express = require('express');
var router = express.Router();
const controller = require('../controllers/movies')

router.get('/movies', controller.findAll)

router.get('/movies/:id', controller.findOne)

router.post('/movies', controller.createOne)

router.put('/movies/:id/:sustituto', controller.update)

router.delete('/movies/:id', controller.del)

module.exports = router;